package com.sapient.test.airports;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AirportTests {

	@DisplayName("Find Airport By Name")
	@Test
	void testFindAirportByName() {
		fail("not yet implemented");
	}

	@DisplayName("Find Airport By Type")
	@Test
	void testFindAirportByType() {
		fail("not yet implemented");
	}

	@DisplayName("Find Airport By City")
	@Test
	void testFindAirportByCity() {
		fail("not yet implemented");
	}

	@DisplayName("Find Airport By Address")
	@Test
	void testFindAirportByAddress() {
		fail("not yet implemented");
	}

	@DisplayName("Find Airport By Lat/Long")
	@Test
	void testFindAirportByLatLong() {
		fail("not yet implemented");
	}

	@DisplayName("Find Airport By NearMe")
	@Test
	void testFindAirportNearMe() {
		fail("not yet implemented");
	}

	@DisplayName("Find Airport Randomly")
	@Test
	void testFindAirportRandomly() {
		fail("not yet implemented");
	}

	@DisplayName("Count of All Airports")
	@Test
	void testCountOfAirports() {
		fail("not yet implemented");
	}

	@DisplayName("List All Airports")
	@Test
	void testListAllAirports() {
		fail("not yet implemented");
	}

}
